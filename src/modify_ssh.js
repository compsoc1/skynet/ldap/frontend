function update_table(keys) {
    let table = document.getElementById("key_table")
    //removing old table
    while (table.rows.length > 0) {
        table.deleteRow(0);
    }
    keys.forEach(key => {
        add_to_table(key);
    })
}

async function get_keys() {
    let username = document.getElementById("user").value;
    let password = document.getElementById("pass").value;
    let request = {auth: {user: username, pass: password}};
    try {
        const response = await fetch('https://api.account.skynet.ie/ldap/ssh', {
            method: 'POST',
            body: JSON.stringify(request),
            mode: 'cors',
        });
        if (response.status !== 200) {
            document.getElementById('formStatus').innerHTML = "<span style='background-color: red; color: white'>Failed to fetch</span>";
            return;
        }
        const data = await response.json();
        if (data.result !== "success") {
            document.getElementById('formStatus').innerHTML = `<span style='background-color: red; color: white'>Error: ${data.error}</span>`;
            return;
        }
        document.getElementById('formStatus').innerHTML = "<span style='background-color: green; color: white'>Success: Got keys</span>";
        let keys = data.success;
        document.getElementById('key_table').style.display = "table";
        update_table(keys)
    } catch (err) {
        document.getElementById('formStatus').innerHTML = `<span style='background-color: red; color: white'>Error: ${err}</span>`;
        console.log(err);
    }
}


async function add_key() {
    let username = document.getElementById("user").value;
    let password = document.getElementById("pass").value;
    let key = document.getElementById("key_input").value;
    let request = {auth: {user: username, pass: password}, key: key};

    try {
        const response = await fetch('https://api.account.skynet.ie/ldap/ssh/add', {
            method: 'POST',
            body: JSON.stringify(request),
            mode: 'cors',
        });
        if (response.status !== 200) {
            document.getElementById('formStatus').innerHTML = "<span style='background-color: red; color: white'>Failed to fetch</span>";
            return;
        }
        const data = await response.json();
        if (data.result !== "success") {
            document.getElementById('formStatus').innerHTML = `<span style='background-color: red; color: white'>Error: ${data.error}</span>`;
            return;
        }
        document.getElementById('formStatus').innerHTML = "<span style='background-color: green; color: white'>Success: Added key </span>";
        let keys = data.success;
        update_table(keys);
    } catch (err) {
        document.getElementById('formStatus').innerHTML = `<span style='background-color: red; color: white'>Error: ${err}</span>`;
        console.log(err);
    }
}

async function delete_key(row_idx, key) {
    let username = document.getElementById("user").value;
    let password = document.getElementById("pass").value;

    let request = {auth: {user: username, pass: password}, key: key};

    try {
        const response = await fetch('https://api.account.skynet.ie/ldap/ssh', {
            method: 'DELETE',
            body: JSON.stringify(request),
            mode: 'cors',
        });
        if (response.status !== 200) {
            document.getElementById('formStatus').innerHTML = "<span style='background-color: red; color: white'>Failed to fetch</span>";
            return;
        }
        const data = await response.json();
        if (data.result !== "success") {
            document.getElementById('formStatus').innerHTML = `<span style='background-color: red; color: white'>Error: ${data.error}</span>`;
            return;
        }
        document.getElementById('formStatus').innerHTML = "<span style='background-color: green; color: white'>Success: Deleted key </span>";
        let keys = data.success;
        update_table(keys);

    } catch (err) {
        document.getElementById('formStatus').innerHTML = `<span style='background-color: red; color: white'>Error: ${err}</span>`;
        console.log(err);
    }
}

function add_to_table(key) {
    let table = document.getElementById("key_table");
    let row = table.insertRow();
    row.setAttribute("ssh_key", key);
    let cell_name = row.insertCell(0);
    let cell_key = row.insertCell(1);

    //This is just for displaying the key, probably can be simplified
    //Splitting by spaces, first two make the key + type, rest is a comment/name of the key
    let key_split = key.split(' ');
    cell_key.textContent = `${key_split[0]} ${key_split[1]}`;
    //Names for SSH keys can have multiple spaces
    let comment = "";
    for (let i = 2; i < key_split.length; i++) {
        comment += " " + key_split[i];
    }
    if (key_split.length === 2) {
        comment = "No comment";
    }
    cell_name.textContent = comment;
    let cell_delete = row.insertCell(2);
    let delete_button = document.createElement("button");
    delete_button.innerHTML = "Delete";

    cell_delete.appendChild(delete_button);
    cell_delete.addEventListener("click", function () {
        delete_key(row.rowIndex, row.getAttribute("ssh_key"));
    });
}

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("ssh_form").addEventListener("submit", function (event) {
        event.preventDefault();
        get_keys();
    });
    document.getElementById("add_key_button").addEventListener("click", add_key);
});
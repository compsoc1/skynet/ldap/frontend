import {request} from "./js/lib.js";

const form = document.getElementById("form");
form.addEventListener('submit',  formHandler);

const form_status = document.getElementById("formStatus");

const button = document.getElementById("button");
button.addEventListener('submit', formHandler);

async function formHandler(listener) {
    listener.preventDefault();
    const formData = new FormData(form);
    const pass = formData.get("pass");

    if (pass !== formData.get("confirm")) {
        form_status.innerHTML = `<span style='background-color: red; color: white'>Passwords don't match</span>`;
        return;
    }


    const urlParam = new URLSearchParams(new URL(window.location.href).search);
    const auth = urlParam.get("auth");

    // if there is no auth value then return here
    if(!auth){
        return;
    }

    const user = formData.get("user");
    const body = {auth: auth, user: user, pass: pass};

    let url = 'https://api.account.skynet.ie/ldap/new/account';
    await request(url, body, form_status, 'POST');
}
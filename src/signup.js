import {request} from "./js/lib.js";

const form = document.getElementById("form");
form.addEventListener('submit',  formHandler);

const form_status = document.getElementById("formStatus");

const button = document.getElementById("button");
button.addEventListener('submit', formHandler);

async function formHandler(listener) {
    listener.preventDefault();
    //HTML below taken from the W3 schools tutorial ()
    form_status.innerHTML = "<div class='loader'></div>"

    const formData = new FormData(form);
    const email = formData.get("email");
    const body = {email: email};


    let url = 'https://api.account.skynet.ie/ldap/new/email';
    await request(url, body, form_status, 'POST');

}
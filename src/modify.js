import {request} from "./js/lib.js";

const form = document.getElementById("form");
form.addEventListener('submit',  formHandler);

const form_status = document.getElementById("formStatus");

const button = document.getElementById("button");
button.addEventListener('submit', formHandler);

const dropdown = document.getElementById("dropdown");
dropdown.addEventListener('onchange', selectField);

async function formHandler(listener) {
    listener.preventDefault();
    const formData = new FormData(form);
    const dropdown_value = dropdown.value;

    if (dropdown_value === "") {
        form_status.innerHTML = "<span style='background-color: red; color: white'>Please select a field to modify</span>";
        return;
    }

    const user = formData.get("user");
    const pass = formData.get("pass");
    const value = formData.get("value");
    const body = {auth: {user: user, pass: pass}, field: dropdown_value, value: value};

    let url = 'https://api.account.skynet.ie/ldap/update';
    await request(url, body, form_status, 'POST');
}

function selectField() {
    if (dropdown.value === 'mail') {
        document.getElementById('value').type = 'email';
    } else {
        document.getElementById('value').type = 'text';
    }
}
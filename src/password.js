import {request} from "./js/lib.js";

const form = document.getElementById("form");
form.addEventListener('submit',  formHandler);

const form_status = document.getElementById("formStatus");

const button = document.getElementById("button");
button.addEventListener('submit', formHandler);

async function formHandler(listener) {
    listener.preventDefault();
    const formData = new FormData(form);
    const user = formData.get("user");
    const pass = formData.get("pass");
    const newPW = formData.get("newPW");
    if (newPW !== formData.get("newPWConfirm")) {

        document.getElementById('formStatus').innerHTML = "<span style='background-color: red; color: white'>Failure: new passwords don't match</span>";

        return;
    }

    const body = {auth: {user: user, pass: pass}, field: "userPassword", value: newPW}

    let url = 'https://api.account.skynet.ie/ldap/update';
    await request(url, body, form_status, 'POST');
}
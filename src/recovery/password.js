const form = document.getElementById("form");
form.addEventListener('submit',  formHandler);

const form_status = document.getElementById("formStatus");

const button = document.getElementById("button");
button.addEventListener('submit', formHandler);

async function formHandler(listener) {
    listener.preventDefault();

    // reset teh form status
    form_status.innerHTML = "<span style='background-color: green; color: white'>Please wait.</span>";

    const formData = new FormData(form);
    const username = formData.get('username').trim();
    const email = formData.get('email').trim();

    if (username.length === 0 && email.length === 0) {
        form_status.innerHTML = "<span style='background-color: red; color: white'>Please enter username or email</span>";
        return;
    }

    let to_send = {
        email: email
    };

    // assuming username is not empty it is the preferred method
    if (username.length > 0) {
        to_send = {user: username};
    }

    let url = "https://api.account.skynet.ie/ldap/recover/password";

    try {
        let req = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(to_send),
            mode: "cors"
        });

        if (req.status === 200) {
            form_status.innerHTML = "<span style='background-color: green; color: white'>Success Please check emails</span>";
        } else {
            form_status.innerHTML = "<span style='background-color: red; color: white'>Failure: Failed to communicate to server</span>";
        }
    } catch (e) {
        form_status.innerHTML = `<span style='background-color: red; color: white'>Error: ${e}</span>`;
    }
}
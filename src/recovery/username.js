import {request} from "../js/lib.js";

const form = document.getElementById("form");
form.addEventListener('submit',  formHandler);

const form_status = document.getElementById("formStatus");

const button = document.getElementById("button");
button.addEventListener('submit', formHandler);


async function formHandler(listener) {
    listener.preventDefault();

    // reset
    form_status.innerHTML = "<span style='background-color: green; color: white'>Please wait.</span>";

    const formData = new FormData(form);
    const email = formData.get("email");
    const body = {email: email};


    let url = 'https://api.account.skynet.ie/ldap/recover/username';
    await request(url, body, form_status, 'POST');
}
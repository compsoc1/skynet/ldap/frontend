import {request} from "../js/lib.js";

const form = document.getElementById("form");
form.addEventListener('submit',  formHandler);

const form_status = document.getElementById("formStatus");

const button = document.getElementById("button");
button.addEventListener('submit', formHandler);

async function formHandler(listener) {
    listener.preventDefault();

    // reset
    form_status.innerHTML = "<span style='background-color: green; color: white'>Please wait.</span>";

    const formData = new FormData(form);
    const pass = formData.get("password");
    const confirm = formData.get("confirm");

    if (pass !== confirm) {
        form_status.innerHTML = "<span style='background-color: red; color: white'>Failure: Passwords don't match</span>";
        return;
    }

    const urlParam = new URLSearchParams(new URL(window.location.href).search);
    const auth = urlParam.get("auth");

    if(!auth){
        return;
    }

    let url = "https://api.account.skynet.ie/ldap/recover/password/auth";
    let body = {auth: auth, pass: pass};

    await request(url, body, form_status, 'POST');
}
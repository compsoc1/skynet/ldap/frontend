import {request} from "../js/lib.js";

const form = document.getElementById("form");
form.addEventListener('submit',  formHandler);

const form_status = document.getElementById("formStatus");

const button = document.getElementById("button");
button.addEventListener('submit', formHandler);


async function formHandler(listener) {
    listener.preventDefault();
    const formData = new FormData(form);
    const body = {user: formData.get('user'), email: formData.get('mail')};

    let url = 'https://api.account.skynet.ie/ldap/recover/ssh/request';
    await request(url, body, form_status, 'POST');
}
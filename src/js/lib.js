export async function request(url, body, form_status, method) {
    try {
        const req = await fetch(url, {
            method: method,
            body: JSON.stringify(body),
            mode: "cors"
        });
        if (req.status !== 200) {
            form_status.innerHTML = "<span style='background-color: red; color: white'>Failure</span>";
        }
        let temp = await req.json();

        if (temp.result === 'error') {
            form_status.innerHTML = `<span style='background-color: red; color: white'>${temp.error}</span>`;
        } else {
            form_status.innerHTML = "<span style='background-color: green; color: white'>Success</span>";
            return true;
        }
    } catch (e) {
        form_status.innerHTML = `<span style='background-color: red; color: white'>${e}</span>`;
    }
    return false;
}
{
  description = "Skynet SSO site";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system:
    let
        pkgs = nixpkgs.legacyPackages."${system}";
    in rec {
      # `nix build`
      defaultPackage = pkgs.stdenv.mkDerivation {
        name = "sso_skynet_ie";
        src = self;
        # buildPhase = "${bfom.defaultPackage."${system}"}/bin/cargo-bfom-blog";
        installPhase = "mkdir -p $out; cp -R src/* $out";
      };

    });
}
